import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import HomePage from "./components/HomePage/index";
import DetailPage from "./components/DetailPage/index"

ReactDOM.render(
    <Router>
        <Switch>
            <Route exact path="/" component={HomePage} />
            <Route exact path="/detail/:id" component={DetailPage} />

        </Switch>
    </Router>,
    document.getElementById("root")
);
serviceWorker.unregister();