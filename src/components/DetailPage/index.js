import React, { Component } from "react";
import ApiList from "../../config/apiUrl";
import axios from "axios";
import defaultImage from "../../assests/images/no-img-portrait-text.png";
import "../../assests/fonts/fonts.css";
import "../../assests/stylesheet/partial.css";
import "./index.css";
class DetailIndex extends Component {
  constructor(props) {
    super(props);
    this.state = {
      detailData: "",
      id: this.props.match.params.id ? this.props.match.params.id : null
    };
  }
  componentDidMount() {
    this.fetchDetails(this.state.id);
  }
  fetchDetails = id => {
    const self = this;
    axios
      .get(ApiList.baseUrl + ApiList.detail + id)
      .then(function(response) {
        // handle success
        if (response && response.data != null) {
          self.setState({
            detailData: response.data
          });
        }
      })
      .catch(function(error) {
        // handle error
        console.log("Error in Detail data -", error);
      });
  };
  render() {
    const { detailData } = this.state;
    return (
      <div>
        <div className="container">
          <div className="row mt-5">
            <div className="col-md-12">
              <h4 className="show-name-heading">
                {detailData.name ? detailData.name : null}
              </h4>
            </div>
          </div>
          {detailData ? (
            <div className="row mt-3 justify-content-center">
              <div className="col-xl-12">
                <div class="card mb-3 shadow-lg detail-card">
                  <div class="row no-gutters">
                    <div class="col-md-3 text-center">
                      <img
                        className="img-fluid"
                        src={
                          detailData.image
                            ? detailData.image.original
                            : defaultImage
                        }
                      />
                    </div>
                    <div class="col-md-9">
                      <div class="card-body">
                        <h5 class="card-title roboto-medium">
                          
                          {detailData.type ? detailData.type : null}

                          <span className="float-right airtime">{
                            detailData.schedule.time? detailData.schedule.time:null
                          }</span>
                        </h5>
                        <div>
                          {detailData.genres
                            ? detailData.genres.map(function(item, i) {
                                return (
                                  <span className="sub-headings">
                                    {i == 0 ? (
                                      <span className="genrer-name text-muted mr-1">
                                        {item}
                                      </span>
                                    ) : (
                                      <span className="genrer-name text-muted mx-1">
                                        | {item}
                                      </span>
                                    )}
                                  </span>
                                );
                              })
                            : null}
                        </div>
                        <div>
                          <small class="text-muted">
                            {detailData.premiered
                              ? detailData.premiered
                              : null}
                          </small>
                          <small class="text-muted ml-2">
                            {detailData.runtime
                              ? detailData.runtime + "min"
                              : null}
                          </small>
                          
                        </div>
                        <p class="card-text mt-4">
                          <div className="sub-info">
                            
                            <span />
                          </div>
                          <div
                            className="content summary-section"
                            dangerouslySetInnerHTML={{
                              __html: detailData.summary
                                ? detailData.summary
                                : null
                            }}
                          />
                        </p>
                        <p class="card-text">
                          <small class="text-muted">
                            <a href={detailData.officialSite}>
                              Vist official site
                            </a>
                          </small>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xl-auto" />
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}
export default DetailIndex;
