import React, { Component } from "react";
import ApiList from "../../config/apiUrl";
import moment from "moment";
import axios from "axios";
import defaultImage from "../../assests/images/no-img-portrait-text.png";
import "../../assests/fonts/fonts.css";
import "../../assests/stylesheet/partial.css"
import "./index.css";
class HomeIndex extends Component {
  constructor() {
    super();
    this.state = {
      listingdata: []
    };
  }
  componentDidMount() {
    const self = this;
    axios
      .get(ApiList.baseUrl + ApiList.homeListing, {
        params: {
          country: "US",
          date: moment().format("YYYY-MM-DD")
        }
      })
      .then(function(response) {
        // handle success
        if (response && response.data.length > 0) {
          self.setState({
            listingdata: response.data
          });
        }
      })
      .catch(function(error) {
        // handle error
        console.log("Error in fetch listing -", error);
      });
  }
  render() {
    const { listingdata } = this.state;
    console.log( JSON.stringify(listingdata))
    return (
      <div>
        <div className="container-fluid px-4">
          <div className="row">
            <div className="col-xl-12">
              <h2 className="mt-5 roboto-medium main-heading">Shows</h2>
            </div>
          </div>
          <div className="row roboto-regular mt-1 show-listing">
            <div className="col-xl-12">
              {listingdata.length>0
                ? listingdata.map(function(item, i) {
                    return (
                      <div className="card-section">
                        <a href={"/detail/"+(item.show.id?item.show.id:null)}>
                          <div className="network-section text-truncate">
                            <span className="heading mx-1">Network :</span>
                            <span>
                              {item.show && item.show.network
                                ? item.show.network.name
                                : null}
                            </span>
                          </div>
                          <div className="show-card mt-1">
                            <img
                              className="show-img h-100 w-100"
                              src={
                                item.show.image
                                  ? item.show.image.original
                                  : defaultImage
                              }
                              alt="show-img"
                            />
                            <div className="air-time">
                              {item.airtime ? item.airtime : null}
                            </div>
                            <div className="detail-section">
                              <div className="show-name text-truncate">
                                {item.show && item.show.network
                                  ? item.show.network.name
                                  : null}
                              </div>
                              <div className="sub-section-detail text-truncate">
                                {item.show.type ? (
                                  <span className="">{item.show.type} </span>
                                ) : null}
                                {item.show.language ? (
                                  <span className="">
                                    
                                    | {item.show.language}
                                  </span>
                                ) : null}

                                {item.show.runtime ? (
                                  <span className="">
                                    
                                    | {item.show.runtime} min
                                  </span>
                                ) : null}
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                    );
                  })
                : "Not able to fetch data"}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default HomeIndex;
